import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

/*import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.config.productionTip = false
import 'bootstrap-vue/dist/bootstrap-vue.css'*/
//import 'bootstrap/dist/css/bootstrap.css'

import vuetify from './plugins/vuetify';
import '@babel/polyfill'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
