import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component:() => import('../views/about.vue')
  },
  {
    path: '/buy',
    name: 'buy',
    component:() => import('../views/buy.vue')
  },
  {
    path: '/sale',
    name: 'sale',
    component:() => import('../views/sale.vue')
  },
  {
    path: '/value',
    name: 'value',
    component:() => import('../views/value.vue')
  }
  //{
    //path: '/about',
    //name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  //}
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
